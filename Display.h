#pragma once
#ifndef _DISPLAY_H
#define _DISPLAY_H

#include "OvenRegistry.h"
#include "Bitmaps.h"

// class Digit 
// {
// 	Digit();
// };
void segH(uint8_t x, uint8_t y, uint8_t len, uint8_t w, uint8_t color)	{display.fillRect(x, y, len, w, color);}
void segV(uint8_t x, uint8_t y, uint8_t len, uint8_t w, uint8_t color)	{display.fillRect(x, y, w, len, color);}

void segA(uint8_t x, uint8_t y, uint8_t len, uint8_t w, uint8_t color)	{segV(x + len-w, y, len, w, color);}
void segB(uint8_t x, uint8_t y, uint8_t len, uint8_t w, uint8_t color)	{segV(x + len-w, y + len-w, len, w, color);}
void segC(uint8_t x, uint8_t y, uint8_t len, uint8_t w, uint8_t color)	{segH(x, y + 2*(len-w), len, w, color);}
void segD(uint8_t x, uint8_t y, uint8_t len, uint8_t w, uint8_t color)	{segV(x, y + (len-w), len, w, color);}
void segE(uint8_t x, uint8_t y, uint8_t len, uint8_t w, uint8_t color)	{segV(x, y, len, w, color);}
void segF(uint8_t x, uint8_t y, uint8_t len, uint8_t w, uint8_t color)	{segH(x, y, len, w, color);}
void segG(uint8_t x, uint8_t y, uint8_t len, uint8_t w, uint8_t color)	{segH(x, y + (len-w), len, w, color);}

void digit7seg(uint8_t digit, uint8_t x, uint8_t y, uint8_t size, uint8_t thickness, uint8_t color)
{
	uint8_t w = thickness;
	uint8_t len = (size + w) / 2;

	switch (digit)
	{
	case 0:
		segA(x, y, len, w, color);
		segB(x, y, len, w, color);
		segC(x, y, len, w, color);
		segD(x, y, len, w, color);
		segE(x, y, len, w, color);
		segF(x, y, len, w, color);
		break;

	case 2:
		segF(x, y, len, w, color);
		segA(x, y, len, w, color);
		segG(x, y, len, w, color);
		segD(x, y, len, w, color);
		segC(x, y, len, w, color);
		break;

	case 4:
		segE(x, y, len, w, color);
		segG(x, y, len, w, color);
		segA(x, y, len, w, color);
		segB(x, y, len, w, color);
		break;

	case 6:
		segD(x, y, len, w, color);
	case 5:
		segF(x, y, len, w, color);
		segE(x, y, len, w, color);
		segG(x, y, len, w, color);
		segB(x, y, len, w, color);
		segC(x, y, len, w, color);
		break;

	case 8:
		segD(x, y, len, w, color);
	case 9:
		segE(x, y, len, w, color);
	case 3:
		segG(x, y, len, w, color);
		segC(x, y, len, w, color);
	case 7:
		segF(x, y, len, w, color);
	case 1:
		segA(x, y, len, w, color);
		segB(x, y, len, w, color);
		break;

	case '-':
		segG(x, y, len, w, color);
	}
}


void printTimer()
{
	if (state.hours)
		digit7seg(state.hours, timerPosX, timerPosY, timerSize, timerThickness, WHITE);
	digit7seg(state.minutes / 10, timerPosX + 2 * timerWidth, timerPosY, timerSize, timerThickness, WHITE);
	digit7seg(state.minutes % 10, timerPosX + 3 * timerWidth, timerPosY, timerSize, timerThickness, WHITE);
	digit7seg(state.seconds / 10, timerPosX + 5 * timerWidth, timerPosY, timerSize, timerThickness, WHITE);
	digit7seg(state.seconds % 10, timerPosX + 6 * timerWidth, timerPosY, timerSize, timerThickness, WHITE);
}


void printTemp(uint16_t temp, uint8_t x, uint8_t y, uint8_t size, uint8_t thickness, uint8_t spacing)
{
	if (temp > 999)
		temp = 999;

	uint8_t temp100s = temp / 100;
	uint8_t temp10s = (temp % 100) / 10;
	uint8_t temp1s = temp % 10;

	if (temp100s > 0)
		digit7seg(temp100s, x, y, size, thickness, WHITE);
	digit7seg(temp10s, x+spacing, y, size, thickness, WHITE);
	digit7seg(temp1s, x+2*spacing, y, size, thickness, WHITE);
}


void printDeriv(int8_t deriv, uint8_t x, uint8_t y, uint8_t size, uint8_t thickness, uint8_t spacing)
{
	if (deriv > 99)
		deriv = 99;
	if (deriv < -99)
		deriv = -99;

	if (deriv < 0)
	{
		digit7seg('-', x, y, size, thickness, WHITE);
		deriv = -deriv;
	}

	uint8_t deriv10s = (deriv % 100) / 10;
	uint8_t deriv1s = deriv % 10;

	digit7seg(deriv10s, x+spacing, y, size, thickness, WHITE);
	display.fillRect(x+2*spacing-2, y+size+1, 2, 2, WHITE);
	digit7seg(deriv1s, x+2*spacing, y, size, thickness, WHITE);
}


void printSettings()
{
	display.setTextSize(1);
	display.setTextColor(WHITE, BLACK);

	display.setCursor(0, 8 * Settings::_speed);
	display.print(F("ctrl speed"));
	display.setCursor(settingValPosX, 8 * Settings::_speed);
	display.print(settings.speed);

	display.setCursor(0, 8 * Settings::_deadzone);
	display.print(F("ctrl deadzone")); //freedom of the control curve
	display.setCursor(settingValPosX, 8 * Settings::_deadzone);
	display.print(settings.deadzone);

	display.setCursor(0, 8 * Settings::_useLearnedControl);
	display.print(F("use learning"));
	display.setCursor(settingValPosX, 8 * Settings::_useLearnedControl);
	display.print(settings.useLearnedControl);

	display.setCursor(0, 8 * Settings::_showDerivative);
	display.print(F("show derivative"));
	display.setCursor(settingValPosX, 8 * Settings::_showDerivative);
	display.print(settings.showDerivative);

	display.setCursor(0, 8 * Settings::_timer);
	display.print(F("manual timer"));
	display.setCursor(settingValPosX, 8 * Settings::_timer);
	display.print(settings.timer);

	display.setCursor(0, 8 * Settings::_useFahrenheit);
	display.print(F("use fahrenheit"));
	display.setCursor(settingValPosX, 8 * Settings::_useFahrenheit);
	display.print(settings.useFahrenheit);

	// display.setCursor(0, 8 * Settings::_ );
	// display.print(F(" "));
	// display.setCursor(settingValPosX, 8 * Settings::_ );
	// display.print(settings. );

	// display.setCursor(0, 8 * Settings::_ );
	// display.print(F(" "));
	// display.setCursor(settingValPosX, 8 * Settings::_ );
	// display.print(settings. );

	if (state.settings_selected)
		display.fillRect(settingValPosX-1, state.settings_cursor * 8, 128-settingValPosX, 8, INVERSE);
	else
		display.fillRect(0, state.settings_cursor * 8, settingValPosX-1, 8, INVERSE);
}
 

void printProfiles()
{
	display.setTextSize(1);
	display.setTextColor(WHITE, BLACK);

	for (uint8_t i=0; i<8; i++)
	{
		display.setCursor(0, 8 * i);
		loadProfileName(i);
		display.print(profile.name);
	}

	display.fillRect(0, state.profiles_cursor * 8, 128, 8, INVERSE);
}


void printMainScreen()
{
	uint16_t primary;
	uint16_t secondary;

	if (settings.useFahrenheit)
	{
		primary = round(toFahrenheit(state.temp));
		secondary = round(toFahrenheit(control.temp_target));
	}
	else
	{
		primary = round(state.temp);
		secondary = round(control.temp_target);
	}

	printTemp(primary,
	          primaryTempPosX,
	          primaryTempPosY,
	          primaryTempSize,
	          primaryTempThickness,
	          primaryTempWidth);

	printTemp(secondary,
	          secondaryTempPosX,
	          secondaryTempPosY,
	          secondaryTempSize,
	          secondaryTempThickness,
	          secondaryTempWidth);


	if (settings.showDerivative)
	{
		int8_t deriv;

		if (settings.useFahrenheit)
			deriv = round(1.8 * 10 * state.temp_deriv);
		else
			deriv = round(10 * state.temp_deriv);

		printDeriv(deriv,
		          secondaryTempPosX,
		          fanPosY,
		          secondaryTempSize,
		          secondaryTempThickness,
		          secondaryTempWidth);
	}
	else if (state.fanON)
		display.drawBitmap(fanPosX, fanPosY, fan, fanSizeX, fanSizeY, WHITE);

	if (state.heatON)
		display.fillRect(secondaryTempPosX-1, secondaryTempPosY-1, 128-secondaryTempPosX+1, secondaryTempPosY + secondaryTempSize+1, INVERSE);


	display.drawFastHLine(0, timerPosY - 4, 128, WHITE);
}


void refreshDisplay()
{
	display.clearDisplay();

	switch (state.mode)
	{
	case State::manual:
		display.setTextSize(1);
		display.setTextColor(WHITE, BLACK);
		display.setCursor(0, 64-8);
		display.print(F("M"));
		
		printMainScreen();

		if (state.running)
			printTimer();
		break;

	case State::profileChoice:
		printProfiles();
		break;

	case State::profile:
		display.setTextSize(1);
		display.setTextColor(WHITE, BLACK);
		display.setCursor(0, 64-8);
		display.print(profile.name);

		printMainScreen();

		printTimer();
		break;

	case State::learning:
		display.setTextSize(1);
		display.setTextColor(WHITE, BLACK);
		display.setCursor(0, 64-8);
		display.print(F("LEARN"));

		printMainScreen();
		break;

	case State::settings:
		printSettings();
		break;
	}

	display.display();
}

#endif//_DISPLAY_H