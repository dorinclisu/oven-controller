# Electric reflow oven controller with thermal parameter learning
## Hardware Architecture
![](documentation/tempdoctor_hardware_architecture.png)

## Software Architecture
![](documentation/tempdoctor_software_architecture.png)

The code definitely needs some refactoring because the relations between modules are a bit convoluted but I don't have the time to maintain this. 
I offer this AS IS and I hope that the diagrams will help you re-use most of the code and adjust it for your needs.