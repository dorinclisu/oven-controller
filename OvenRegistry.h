#pragma once
#ifndef _OVENREGISTRY_H
#define _OVENREGISTRY_H

#include "Parameters.h"
#include <Adafruit_SSD1306.h>
#include "Adafruit_MAX31856.h"

volatile struct Settings
{
	float speed;
	float deadzone;
	bool useLearnedControl;
	bool showDerivative;
	bool useFahrenheit;

	uint16_t timer;
	//control the display order of the setting items
	enum {_speed, _deadzone, _useLearnedControl, _showDerivative, _useFahrenheit, _timer}; //max 8 items !!!

	bool learned;

	uint16_t signature;
} settings;


volatile struct State
{
	float temp;
	float temp_raw;
	float temp_deriv;

	bool heatON;
	bool fanON;

	bool running;
	bool wasRunning;
	enum running_mode {manual, profileChoice, profile, settings, learning, error} mode;
	
	//menu logic
	uint8_t settings_cursor;
	uint8_t profiles_cursor;
	bool settings_selected;

	bool loadProfile;
	bool saveSettings;
	bool refreshDisplay;
	bool clearDisplay;
	bool clickSound;
	bool tempReady;
	bool beeping;

	//misc
	uint32_t start_time;
	uint8_t hours, minutes, seconds;
} state;


volatile struct Control
{
	float temp_target;
	float temp_deriv_target;
} control;


struct Learning
{
	float k [2][LEARN_NODE_COUNT];
	float t [2][LEARN_NODE_COUNT];
} learning;


struct Profile
{
	char name[16];
	uint8_t temp[64];
	uint8_t length;
	uint8_t interval;
} profile;

///////////////////////////////////////
float tbuff [TEMP_FILTER_LEN];
float tdbuff [TEMP_DERIV_FILTER_LEN];
MovingAverage tfilter(tbuff, TEMP_FILTER_LEN);
MovingAverage tdfilter(tdbuff, TEMP_DERIV_FILTER_LEN);

CAP1298 touch;
Adafruit_SSD1306 display; //hw i2c
Adafruit_MAX31856 sensor(MAX31856_NCS_PIN, MAX31856_SDI_PIN, MAX31856_SDO_PIN, MAX31856_SCK_PIN);
///////////////////////////////////////
void log();
void heatON();
void heatOFF();
void updateTimer();
void applyControl();
void learn();
void readTemperature();
void readDerivative();
void tempError();
float getOptimumDerivative(float t_set, float t_meas);
void clickSound();
void loadSettings();
void saveSettings();
void saveLearning();
void loadLearning();
void saveProfile();
void loadProfile();
void loadProfileName(uint8_t cursor);


#endif//_OVENREGISTRY_H