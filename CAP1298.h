/*
MIT License

Copyright (c) 2018 Dorin Clisu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once
#ifndef _CAP1298_H
#define _CAP1298_H

#define CAP_ADDRESS_READ 0x51
#define CAP_ADDRESS_WRITE 0x50

#define CAP_ID 0x71
#define CAP_MID 0xFE
#define CAP_REV 0xFF

#define CAP_MAIN_CONTROL 0x00
#define CAP_GENERAL_STATUS 0x02
#define CAP_INPUT_STATUS 0x03

#define CAP_INPUT_DELTA_1 0x10
#define CAP_INPUT_DELTA_2 0x11
#define CAP_INPUT_DELTA_3 0x12
#define CAP_INPUT_DELTA_4 0x13
#define CAP_INPUT_DELTA_5 0x14
#define CAP_INPUT_DELTA_6 0x15
#define CAP_INPUT_DELTA_7 0x16
#define CAP_INPUT_DELTA_8 0x17

#define CAP_SENSITIVITY 0x1F
#define CAP_CONFIGURATION 0x20
#define CAP_CONFIGURATION2 0x44
#define CAP_INPUT_ENABLE 0x21
#define CAP_INPUT_CONFIG 0x22
#define CAP_INPUT_CONFIG2 0x23
#define CAP_SAMPLING_CONFIG 0x24
#define CAP_CALIBRATION 0x26
#define CAP_IRQ_ENABLE 0x27
#define CAP_REPEAT_ENABLE 0x28
#define CAP_SIG_GUARD_ENABLE 0x29

#define CAP_MT_CONFIG 0x2A
#define CAP_MT_PATTERN_CONFIG 0x2B
#define CAP_MT_PATTERN 0x2D

#define CAP_BASE_COUNT_LIMIT 0x2E
#define CAP_RECALIBRATION_CONFIG 0x2F

#define CAP_INPUT_TRESH_1 0x30
#define CAP_INPUT_TRESH_2 0x31
#define CAP_INPUT_TRESH_3 0x32
#define CAP_INPUT_TRESH_4 0x33
#define CAP_INPUT_TRESH_5 0x34
#define CAP_INPUT_TRESH_6 0x35
#define CAP_INPUT_TRESH_7 0x36
#define CAP_INPUT_TRESH_8 0x37

#define CAP_NOISE_STATUS 0x0A
#define CAP_INPUT_NOISE_TRESH 0x38

#define CAP_STANDBY_CHANNEL 0x40
#define CAP_STANDBY_CONFIG 0x41
#define CAP_STANDBY_SENSITIVITY 0x42
#define CAP_STANDBY_TRESH 0x43

#define CAP_INPUT_BASE_CNT_1 0x50
#define CAP_INPUT_BASE_CNT_2 0x51
#define CAP_INPUT_BASE_CNT_3 0x52
#define CAP_INPUT_BASE_CNT_4 0x53
#define CAP_INPUT_BASE_CNT_5 0x54
#define CAP_INPUT_BASE_CNT_6 0x55
#define CAP_INPUT_BASE_CNT_7 0x56
#define CAP_INPUT_BASE_CNT_8 0x57

#define CAP_POWER_BUTTON 0x60
#define CAP_POWER_BUTTON_CONFIG 0x61

#define CAP_CALIB_SENS_CONFIG1 0x80
#define CAP_CALIB_SENS_CONFIG2 0x81

#define CAP_INPUT_CALIB_1 0xB1
#define CAP_INPUT_CALIB_2 0xB2
#define CAP_INPUT_CALIB_3 0xB3
#define CAP_INPUT_CALIB_4 0xB4
#define CAP_INPUT_CALIB_5 0xB5
#define CAP_INPUT_CALIB_6 0xB6
#define CAP_INPUT_CALIB_7 0xB7
#define CAP_INPUT_CALIB_8 0xB8
#define CAP_INPUT_CALIB_LSB1 0xB9
#define CAP_INPUT_CALIB_LSB2 0xBA
///////////////////////////////////////

#if defined(__AVR__)
#define SDA_PORT CAP1298_SDA_PORT
#define SDA_PIN CAP1298_SDA_BIT 
#define SCL_PORT CAP1298_SCL_PORT
#define SCL_PIN CAP1298_SCL_BIT
#define I2C_FASTMODE true
#include "SoftI2CMaster.h"

#else
#include "SlowSoftI2CMaster.h"
#pragma message "Compiling with 'SlowSoftI2CMaster.h'"
#endif
 
#if defined(__AVR__)
class CAP1298
{
public:
	CAP1298() {};

#else
class CAP1298 : public SlowSoftI2CMaster
{
public:
	CAP1298(const uint8_t sda, const uint8_t scl) : SlowSoftI2CMaster(sda, scl) {};

#endif
	void init();

	void write_byte(uint8_t address, uint8_t byte);
	uint8_t read_byte(uint8_t address);

	uint8_t get_touched_inputs();
	int8_t get_channel_delta(uint8_t channel);
	void read_channel_deltas();
	void clear_irq_flag();
	void set_irq_on_release(bool enabledOnRelease);
	void set_enabled_inputs(uint8_t flags);
	void enable_input(uint8_t channel);
	void disable_input(uint8_t channel);

	void set_repeat_inputs(uint8_t flags);
	void enable_repeat_input(uint8_t channel);
	void disable_repeat_input(uint8_t channel);
	void set_repeat_period(uint8_t factor); //period = 35ms * factor
	void set_minimum_hold(uint8_t factor); //hold = 35ms * factor

	void set_analog_gain(uint8_t exponent); //gain = 2^exponent
	void set_digital_gain(uint8_t exponent); //gain = 2^exponent
	void set_channel_treshold(uint8_t channel, uint8_t treshold);
	void set_average_sample_count(uint8_t exponent); //count = 2^exponent
	void set_sampling_time(uint8_t exponent); //time = 320us * 2^exponent
	void set_cycle_time(uint8_t factor); //time = 35ms * factor


	int8_t channelDelta[8];
};










void CAP1298::write_byte(uint8_t address, uint8_t byte)
{
	i2c_start(CAP_ADDRESS_WRITE);
	i2c_write(address);
	i2c_write(byte);
	i2c_stop();
}

uint8_t CAP1298::read_byte(uint8_t address)
{
	i2c_start(CAP_ADDRESS_WRITE);
	i2c_write(address);
	i2c_rep_start(CAP_ADDRESS_READ);
	uint8_t byte = i2c_read(true);
	i2c_stop();
	return byte;
}


void CAP1298::clear_irq_flag()
{
	uint8_t main_control_reg = read_byte(CAP_MAIN_CONTROL);
	main_control_reg &= ~1;
	write_byte(CAP_MAIN_CONTROL, main_control_reg);
}

uint8_t CAP1298::get_touched_inputs()
{
	return read_byte(CAP_INPUT_STATUS);
}

int8_t CAP1298::get_channel_delta(uint8_t channel)
{
	channel--;
	if (channel > 7)
		channel = 7;

	uint8_t ADDRESS = CAP_INPUT_DELTA_1 + channel;
	return read_byte(ADDRESS);
}

void CAP1298::read_channel_deltas()
{
	uint8_t address = CAP_INPUT_DELTA_1;

	i2c_start(CAP_ADDRESS_WRITE);
	i2c_write(address);
	i2c_rep_start(CAP_ADDRESS_READ);
	channelDelta[0] = i2c_read(false);
	channelDelta[1] = i2c_read(false);
	channelDelta[2] = i2c_read(false);
	channelDelta[3] = i2c_read(false);
	channelDelta[4] = i2c_read(false);
	channelDelta[5] = i2c_read(false);
	channelDelta[6] = i2c_read(false);
	channelDelta[7] = i2c_read(true);
	i2c_stop();
}

void CAP1298::set_irq_on_release(bool enabledOnRelease)
{
	uint8_t config2_reg = read_byte(CAP_CONFIGURATION2);
	config2_reg &= ~1;
	config2_reg |= not enabledOnRelease;
	write_byte(CAP_CONFIGURATION2, config2_reg);
}

void CAP1298::set_enabled_inputs(uint8_t flags)
{
	write_byte(CAP_INPUT_ENABLE, flags);
}

void CAP1298::enable_input(uint8_t channel)
{
	channel--;

	uint8_t input_enable_reg = read_byte(CAP_INPUT_ENABLE);
	input_enable_reg |= (1 << channel);
	write_byte(CAP_INPUT_ENABLE, input_enable_reg);
}

void CAP1298::disable_input(uint8_t channel)
{
	channel--;
	
	uint8_t input_enable_reg = read_byte(CAP_INPUT_ENABLE);
	input_enable_reg &= ~(1 << channel);
	write_byte(CAP_INPUT_ENABLE, input_enable_reg);
}

void CAP1298::set_repeat_inputs(uint8_t flags)
{
	write_byte(CAP_REPEAT_ENABLE, flags);
}

void CAP1298::enable_repeat_input(uint8_t channel)
{
	channel--;

	uint8_t repeat_enable_reg = read_byte(CAP_REPEAT_ENABLE);
	repeat_enable_reg |= (1 << channel);
	write_byte(CAP_REPEAT_ENABLE, repeat_enable_reg);
}

void CAP1298::disable_repeat_input(uint8_t channel)
{
	channel--;

	uint8_t repeat_enable_reg = read_byte(CAP_REPEAT_ENABLE);
	repeat_enable_reg &= ~(1 << channel);
	write_byte(CAP_REPEAT_ENABLE, repeat_enable_reg);	
}

void CAP1298::set_repeat_period(uint8_t factor) //period = 35ms * factor
{
	factor--;
	if (factor > 15)
		factor = 15;

	uint8_t input_config_reg = read_byte(CAP_INPUT_CONFIG);
	input_config_reg &= ~15;
	input_config_reg |= factor;
	write_byte(CAP_INPUT_CONFIG, input_config_reg);
}

void CAP1298::set_minimum_hold(uint8_t factor) //hold = 35ms * factor
{
	factor--;
	if (factor > 15)
		factor = 15;

	uint8_t input_config2_reg = read_byte(CAP_INPUT_CONFIG2);
	input_config2_reg &= ~15;
	input_config2_reg |= factor;
	write_byte(CAP_INPUT_CONFIG2, input_config2_reg);
}

void CAP1298::set_analog_gain(uint8_t exponent) //gain = 2^exponent
{
	if (exponent > 3)
		exponent = 3;

	uint8_t main_control_reg = read_byte(CAP_MAIN_CONTROL);
	main_control_reg &= ~(3 << 6);
	main_control_reg |= (exponent << 6);
	write_byte(CAP_MAIN_CONTROL, main_control_reg);
}

void CAP1298::set_digital_gain(uint8_t exponent) //gain = 2^exponent
{
	if (exponent > 7)
		exponent = 7;
	exponent = 7 - exponent;

	uint8_t sensitivity_reg = read_byte(CAP_SENSITIVITY);
	sensitivity_reg &= ~(7 << 4);
	sensitivity_reg |= (exponent << 4);
	write_byte(CAP_SENSITIVITY, sensitivity_reg);
}

void CAP1298::set_channel_treshold(uint8_t channel, uint8_t treshold)
{
	if (treshold > 127)
		treshold = 127;

	channel--;
	if (channel > 7)
		channel = 7;
	uint8_t ADDRESS = CAP_INPUT_TRESH_1 + channel;

	write_byte(ADDRESS, treshold);
}

void CAP1298::set_average_sample_count(uint8_t exponent) //count = 2^exponent
{
	if (exponent > 7)
		exponent = 7;

	uint8_t sampling_config_reg = read_byte(CAP_SAMPLING_CONFIG);
	sampling_config_reg &= ~(7 << 4);
	sampling_config_reg |= (exponent << 4);
	write_byte(CAP_SAMPLING_CONFIG, sampling_config_reg);
}

void CAP1298::set_sampling_time(uint8_t exponent) //time = 320us * 2^exponent
{
	if (exponent > 3)
		exponent = 3;

	uint8_t sampling_config_reg = read_byte(CAP_SAMPLING_CONFIG);
	sampling_config_reg &= ~(3 << 2);
	sampling_config_reg |= (exponent << 2);
	write_byte(CAP_SAMPLING_CONFIG, sampling_config_reg);
}

void CAP1298::set_cycle_time(uint8_t factor) //time = 35ms * factor
{
	factor--;
	if (factor > 3)
		factor = 3;

	uint8_t sampling_config_reg = read_byte(CAP_SAMPLING_CONFIG);
	sampling_config_reg &= ~3;
	sampling_config_reg |= factor;
	write_byte(CAP_SAMPLING_CONFIG, sampling_config_reg);
}


void CAP1298::init()
{
	delay(15);

	i2c_init();
	clear_irq_flag();

	// set_analog_gain(0);
	// set_digital_gain(5);
	// set_channel_treshold(1, 64);
	// set_channel_treshold(2, 64);
	// set_channel_treshold(3, 64);
	// set_channel_treshold(4, 64);
	// set_channel_treshold(5, 64);
	// set_channel_treshold(6, 64);
	// set_channel_treshold(7, 64);
	// set_channel_treshold(8, 64);

	// set_average_sample_count(3);
	// set_sampling_time(2);
	// set_cycle_time(2);

	// set_irq_on_release(true);

	// set_enabled_inputs(0b11111111);

	// set_repeat_inputs(0b11111111);
	// set_repeat_period(5);
	// set_minimum_hold(8);
}

#endif//_CAP1298_H
