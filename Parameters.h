#pragma once
#ifndef _PARAMETERS_H
#define _PARAMETERS_H

#define DEBUG

#define EXP_CONTROL_MODEL
//#define POW_CONTROL_MODEL

//hardware stuff
#define MAX31856_NCS_PIN A0
#define MAX31856_SCK_PIN 15
#define MAX31856_SDI_PIN 16
#define MAX31856_SDO_PIN 14
#define MAX31856_NDRDY_PIN 7 
//#define MAX31856_NFAULT_PIN

#define CAP1298_SDA_PORT PORTF
#define CAP1298_SDA_BIT 5
#define CAP1298_SCL_PORT PORTF
#define CAP1298_SCL_BIT 4
#define CAP1298_ALERT_PIN 0

#define SOUND_PIN 10
#define HEAT_PIN 6
#define FAN_PIN 5
#define LED_PIN 4

#define OLED_RST_PIN 20
#define OLED_SDA_PIN 2
#define OLED_SCL_PIN 3
//TODO: separate parameters that can be changed by users without understanding the whole code

float toFahrenheit(float c) {return (c + 40) * 1.8 - 40;}
float toCelsius(float f) {return (f + 40) / 1.8 - 40;}

const char VERSION [] = __DATE__;
//VERSION [11] = '\0';

//logic stuff
const uint8_t TEMP_MIN = 60;
const uint8_t TEMP_MAX = 240; //for supporting temperatures above 255 more memory needs to be allocated (don't mess with this)

const uint8_t LEARN_NODE_COUNT = 2;
const uint8_t LEARN_TEMP_NODES [LEARN_NODE_COUNT] = {100, 180};
const uint8_t LEARN_NODE_JUMP = 10;

const float K_DEFAULT = 0.05; //optimum_deriv = -t_err * K

const uint8_t TEMP_FILTER_LEN = 8; //filtered value lags behind _FILTER_LEN / 2 * _UPDATE_PERIOD //ex.: 8/2 * 100ms = 400ms
const uint8_t TEMP_DERIV_FILTER_LEN = 8; //same lag formula

//const uint16_t PROFILE_INTERVAL = 10; //[s]

const uint8_t TIMER_INCREMENT = 10; //[s]

const uint16_t CONTROL_UPDATE_PERIOD = 100; //[ms] //must be slow enough to accomodate temp sensor speed

const uint16_t BEEP_INTERVAL = 250; //[ms]
const uint8_t DOUBLE_TAP_INTERVAL = 200; //[ms]


//memory stuff
const uint16_t MEM_SIGNATURE = 42; //identifies a specific memory format for saving the settings
const uint16_t settingsAddress = 0;
const uint16_t profilesAddress = 64;
const uint16_t learningAddress = 900;

//display stuff
const uint8_t primaryTempPosX = 0;
const uint8_t primaryTempPosY = 0;
const uint8_t primaryTempSize = 40;
const uint8_t primaryTempThickness = 4;
const uint8_t primaryTempSpacing = 6;
const uint8_t  primaryTempWidth = (primaryTempSize + primaryTempThickness) / 2 + primaryTempSpacing;

const uint8_t secondaryTempPosX = 94;
const uint8_t secondaryTempPosY = 1;
const uint8_t secondaryTempSize = 16;
const uint8_t secondaryTempThickness = 2;
const uint8_t secondaryTempSpacing = 3;
const uint8_t secondaryTempWidth = (secondaryTempSize + secondaryTempThickness) / 2 + secondaryTempSpacing;

const uint8_t timerSize = 9;
const uint8_t timerThickness = 1;
const uint8_t timerSpacing = 2;
const uint8_t timerWidth = (timerSize + timerThickness) / 2 + timerSpacing;
const uint8_t timerPosX = 128 - (1+1+2+1+2) * timerWidth + timerSpacing;
const uint8_t timerPosY = 64 - timerSize;
const uint8_t derivPosX = 16;

const uint8_t fanSizeX = 16;
const uint8_t fanSizeY = 16;
const uint8_t fanPosX = 128 - fanSizeX;
const uint8_t fanPosY = primaryTempSize - fanSizeY;

const uint8_t heaterSizeX = 15;

const uint8_t settingValPosX = 128 - (1+6*4);


#endif//_PARAMETERS_H