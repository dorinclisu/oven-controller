#pragma once
#ifndef _UIMANAGER_H
#define _UIMANAGER_H

//#include <Arduino.h>
//#include <wirish.h>
#include "OvenRegistry.h"


inline void settings_up()
{
	if (state.settings_selected)
	{
		switch (state.settings_cursor)
		{
		case Settings::_speed:
			settings.speed += 0.01;
			break;

		case Settings::_deadzone:
			settings.deadzone += 0.01;
			break;

		case Settings::_useLearnedControl:
			if (settings.learned)
				settings.useLearnedControl = true;
			break;

		case Settings::_showDerivative:
			settings.showDerivative = true;
			break;

		case Settings::_timer:
			if (settings.timer < 9999 - TIMER_INCREMENT)
				settings.timer += TIMER_INCREMENT;
			break;

		case Settings::_useFahrenheit:
			settings.useFahrenheit = true;
			break;

		default:
			state.settings_selected = false;
		}
	}
	else
	{
		if (state.settings_cursor > 0)
			state.settings_cursor --;
	}
}

inline void settings_dn()
{
	if (state.settings_selected)
	{
		switch (state.settings_cursor)
		{
		case Settings::_speed:
			settings.speed -= 0.01;
			break;

		case Settings::_deadzone:
			settings.deadzone -= 0.01;
			break;

		case Settings::_useLearnedControl:
			settings.useLearnedControl = false;
			break;

		case Settings::_showDerivative:
			settings.showDerivative = false;
			break;

		case Settings::_timer:
			if (settings.timer > 0)
				settings.timer -= 10;
			break;

		case Settings::_useFahrenheit:
			settings.useFahrenheit = false;
			break;

		default:
			state.settings_selected = false;
		}
	}
	else
	{
		if (state.settings_cursor < 7)
			state.settings_cursor ++;
	}
}

inline void settings_select()
{
	state.settings_selected = not state.settings_selected;
}


inline void profiles_up()
{
	if (state.profiles_cursor > 0)
		state.profiles_cursor --;
}

inline void profiles_dn()
{
	if (state.profiles_cursor < 7)
		state.profiles_cursor ++;
}

inline void profiles_select()
{
	state.mode = State::profile;
	state.loadProfile = true;
	state.clearDisplay = true;
}


///////////////////////////////////////////////////////////////////////////////
inline void touch_setting()
{
	static uint8_t previous = State::manual;

	switch (state.mode)
	{
		case State::manual:
			previous = state.mode;
			state.mode = State::settings;
			state.clearDisplay = true;
			break;

		case State::profile:
			if (not state.running)
			{
				previous = state.mode;
				state.mode = State::settings;
				state.clearDisplay = true;
			}
			break;

		case State::settings:
			state.mode = previous;
			state.clearDisplay = true;
			state.saveSettings = true;
			break;
	}
}

inline void touch_release_setting()
{

}
 
inline void touch_fan()
{
	switch (state.mode)
	{
		case State::manual:
			if (not state.running)
				state.fanON = not state.fanON;
			break;

		case State::profile:
			if (not state.running)
				state.fanON = not state.fanON;
			break;

		case State::settings:
			break;
	}
}

inline void touch_S()
{
	static uint32_t last_time;

	switch (state.mode)
	{
		case State::manual:
			if (millis() - last_time < DOUBLE_TAP_INTERVAL)
				state.running = not state.running; //temporary
			else
				last_time = millis();
			break;

		case State::profile:
			if (millis() - last_time < DOUBLE_TAP_INTERVAL)
				state.running = not state.running; //temporary
			else
				last_time = millis();
			break;

		case State::profileChoice:
			profiles_select();
			break;

		case State::settings:
			settings_select();
			break;
	}
}

inline void touch_release_S()
{

}

inline void touch_up()
{
	switch (state.mode)
	{
		case State::manual:
			control.temp_target ++;
			if (control.temp_target > TEMP_MAX)
				control.temp_target = TEMP_MAX;
			break;

		case State::profile:
			break;

		case State::profileChoice:
			profiles_up();
			break;

		case State::settings:
			settings_up();
			break;
	}
}

inline void touch_dn()
{
	switch (state.mode)
	{
		case State::manual:
			control.temp_target --;
			if (control.temp_target < TEMP_MIN)
				control.temp_target = TEMP_MIN;
			break;

		case State::profile:
			break;

		case State::profileChoice:
			profiles_dn();
			break;

		case State::settings:
			settings_dn();
			break;
	}
}

inline void touch_M()
{
	switch (state.mode)
	{
		case State::manual:
			break;

		case State::profile:
			if (not state.running)
			{
				state.mode = State::manual;
				state.clearDisplay = true;
			}
			break;

		case State::profileChoice:
			state.mode = State::manual;
			state.clearDisplay = true;
			break;

		case State::settings:
			break;
	}
}

inline void touch_P()
{
	switch (state.mode)
	{
		case State::manual:
			if (not state.running)
			{
				state.mode = State::profileChoice;
				state.clearDisplay = true;
			}
			break;

		case State::profile:
			if (not state.running)
			{
				state.mode = State::profileChoice;
				state.clearDisplay = true;
			}
			break;

		case State::settings:
			if (state.settings_cursor == Settings::_useLearnedControl and state.settings_selected)
			{
				state.mode = State::learning;
				state.clearDisplay = true;
			}
			break;
	}
}

///////////////////////////////////////////////////////////////////////////////
void handleTouchEvent()
{
	touch.clear_irq_flag();
	uint8_t inputs = touch.get_touched_inputs();

	switch (inputs)
	{
		case 1:
		touch_M();
		break;

		case 2:
		touch_fan();
		break;

		case 4:
		touch_setting();
		break;

		case 16:
		touch_S();
		break;

		case 32:
		touch_up();
		break;

		case 64:
		touch_dn();
		break;

		case 128:
		touch_P();
		break;
	}

	clickSound();

	state.refreshDisplay = true;
	state.clickSound = true;
	state.beeping = false;
}

#endif//_UIMANAGER_H