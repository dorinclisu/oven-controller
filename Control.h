/*
MIT License

Copyright (c) 2018 Dorin Clisu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once
#ifndef _CONTROL_H
#define _CONTROL_H

#include "OvenRegistry.h"


//TODO: make it a non-blocking function; the loop should be open and the function return true when it's done
void learn() //experimental
{
	state.running = true;
	float speed_backup = settings.speed;
	settings.speed = 0.5;
	settings.useLearnedControl = false;
	loadLearning();

	float derivative;
	uint32_t time;

	for (uint8_t i=0; i < LEARN_NODE_COUNT; i++)
	{
		control.temp_target = LEARN_TEMP_NODES [i] - LEARN_NODE_JUMP;

		while (state.temp < control.temp_target)
		{
			if (state.tempReady)
			{
				state.tempReady = false;
				control.temp_deriv_target = getOptimumDerivative(control.temp_target, state.temp);
				applyControl();
				refreshDisplay();
				log();
			}
		}

		control.temp_target = LEARN_TEMP_NODES [i];
		heatON();

		while (state.temp < control.temp_target)
		{
			if (state.tempReady)
			{
				state.tempReady = false;
				refreshDisplay();
				log();
			}
		}

		heatOFF();
		derivative = state.temp_deriv;
		time = millis();

		while (state.temp_deriv > 0)
		{
			if (state.tempReady)
			{
				state.tempReady = false;
				refreshDisplay();
				log();
			}
		}

		float delta_t = static_cast<float> (millis() - time) / 1000;

#ifdef EXP_CONTROL_MODEL
		learning.t [0] [i] = state.temp;
		learning.k [0] [i] = derivative / (state.temp - control.temp_target);

		learning.t [1] [i] = state.temp;
		learning.k [1] [i] = log(state.temp - control.temp_target) / delta_t;
		// f(x) = e^kx  and  k = log(f(x)) / x  =>  g(x) = kx  such that  g(f(x)) = f'(x)
#else
		learning.t [0] [i] = state.temp;
		learning.k [0] [i] = derivative * derivative / (4 * (state.temp - control.temp_target));

		learning.t [1] [i] = state.temp;
		learning.k [1] [i] = (state.temp - control.temp_target) / (delta_t * delta_t);
		// f(x) = kx^2  and  k = f(x) / x^2  =>  g(x) = 2sqrt(kx)  such that  g(f(x)) = f'(x)
#endif
	}

	settings.learned = true;
	settings.useLearnedControl = true;
	settings.speed = speed_backup;
	saveLearning();
	saveSettings();

#ifdef DEBUG
	// memset((void*)(&learning), 0, sizeof(learning));
	// loadLearning();

	// Serial.print(F("\nt0\tk0x100\n"));
	// for (int i=0; i < LEARN_NODE_COUNT; i++)
	// {
	// 	Serial.print(learning.t [0][i]);
	// 	Serial.print('\t');
	// 	Serial.println(100*learning.k [0][i]);
	// }
	// Serial.print(F("t1\tk1x100\n"));
	// for (int i=0; i < LEARN_NODE_COUNT; i++)
	// {
	// 	Serial.print(learning.t [1][i]);
	// 	Serial.print('\t');
	// 	Serial.println(100*learning.k [1][i]);
	// }
#endif//DEBUG

	control.temp_target = TEMP_MIN;
	state.running = false;
}


//TODO: improve the derivative curve computation (from simple linear model)
float getOptimumDerivative(float t_set, float t_meas)
{
	float t_err = t_meas - t_set;
	float derivative;

	if (settings.learned and settings.useLearnedControl and (LEARN_NODE_COUNT == 2))
	{
		float deriv[2];
		float temp[2];

		temp[0] = learning.t [state.fanON][0];
		temp[1] = learning.t [state.fanON][1];

#ifdef EXP_CONTROL_MODEL
		deriv[0] = -t_err * learning.k [state.fanON][0];
		deriv[1] = -t_err * learning.k [state.fanON][1];
#else
		deriv[0] = -t_err / abs(t_err) * 2 * sqrt(abs(t_err) * learning.k [state.fanON][0]);
		deriv[1] = -t_err / abs(t_err) * 2 * sqrt(abs(t_err) * learning.k [state.fanON][0]);
#endif
		derivative = deriv[0] + (t_set - temp[0]) * (deriv[1] - deriv[0]) / (temp[1] - temp[0]);
	}
	else
	{
		derivative = -t_err * K_DEFAULT;
	}

	return derivative;
}


void applyControl()
{
	if (state.running)
	{
		float target = control.temp_deriv_target * settings.speed;

		if (state.temp_deriv < target and state.temp < TEMP_MAX)
			heatON();
		else
			heatOFF();
	}
	else
		heatOFF();
}


#endif//_CONTROL_H