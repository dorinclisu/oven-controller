import sys
import serial


if (len(sys.argv) >= 1):
	pname = sys.argv[0]
else:
	pname = "/dev/cu.zenon_1-DevB"

if (len(sys.argv) >= 2):
	fname = sys.argv[1]
else:
	fname = "plot.txt"


port = serial.Serial(port=pname, baudrate=57400)
file = open(fname, "w")


while True:
	file.write(port.read())
