import time
import math
# pip install --user http://bit.ly/csc161graphics
from graphics import *


def toKelvin(t):
	return t - 273.15

def toCelsius(t):
	return t + 273.15

def computeTargetDerivative(t_oven, t_set):
	t_err = t_oven - t_set

	if (t_err < 0):
		return - t_err**1.5 / 30
	else:
		return - t_err**2 / 10


def main():
	win = GraphWin("Electric Oven Simulator", 800, 600) # give title and dimensions
	win.setBackground("black")

	file = open("sim.txt", "w")

	###########################################################################
	sigma = 5.67 * pow(10, -8) # boltzmann's constant
	h = 7 # heat transfer coefficient (air convection)
	c_steel = 452.0 # specific heat capacity of steel

	electric_power = 1100.0 # power of the oven heater

	m_heater = 0.1 # mass
	m_oven = 1.5

	e_heater = 0.95 # thermal emissivity
	e_oven = 0.1

	A_heater = 4 * 0.25 * math.pi * 0.0066 # surface area
	A_oven = 2 * 0.3 * 0.25 + 2 * 0.3 * 0.25 + 2 * 0.25 * 0.25

	sigma_heater = e_heater * A_heater * sigma
	sigma_oven = e_oven * A_oven * sigma

	h_heater = A_heater * h * 4
	h_oven = A_oven * h

	c_heater = m_heater * c_steel # heat capacity
	c_oven = m_oven * c_steel

	t_ambient = toKelvin(20.0)

	t_heater = t_ambient
	t_oven = t_ambient

	duty = 0.0
	t_set = t_ambient
	###########################################################################

	gaugePower = Text(Point(10, 10), "0")
	gaugeHeater = Text(Point(110, 10), t_ambient)
	gaugeOven = Text(Point(210, 10), t_ambient)
	gaugeDeriv = Text(Point(310, 10), 0)

	gaugePower.setFill("white")
	gaugeHeater.setFill("white")
	gaugeOven.setFill("white")
	gaugeDeriv.setFill("white")

	gaugePower.draw(win)
	gaugeHeater.draw(win)
	gaugeOven.draw(win)
	gaugeDeriv.draw(win)

	# inputBox = Entry(Point(410, 10), 3)
	# inputBox.setFill("black")
	# inputBox.setTextColor("white")
	# inputBox.draw(win)

	t_prev = time.time()
	t_prev_plot = t_prev;
	deriv_oven_prev = 0

	x = 0 # plotting coordinate

	while True :
		time.sleep(0.01)
		t_now = time.time() 
		dt = t_now - t_prev
		t_prev = t_now

		heater_radiation = sigma_heater * (pow(t_heater, 4) - pow(t_oven, 4))
		heater_convection = h_heater * (t_heater - t_oven)
		oven_radiation = sigma_oven * (pow(t_oven, 4) - pow(t_ambient, 4))
		oven_convection = h_oven * (t_oven - t_ambient)

		deriv_heater = (electric_power * duty - heater_radiation - heater_convection) / c_heater
		deriv_oven = (heater_radiation + heater_convection - oven_radiation - oven_convection) / c_oven

		t_heater += dt * deriv_heater
		t_oven += dt * deriv_oven

		dderiv_oven = (deriv_oven - deriv_oven_prev) / dt
		deriv_oven_prev = deriv_oven

		#######################################################################
		gaugeDeriv.setText("{:.2f}".format(deriv_oven))
		gaugeHeater.setText("{:.2f}".format(toCelsius(t_heater)))
		gaugeOven.setText("{:.2f}".format(toCelsius(t_oven)))
		gaugePower.setText("{:.2f}".format(duty))

		if (t_now - t_prev_plot > 0.25):
			t_prev_plot = t_now
			win.plot(x, 600 - int(2*toCelsius(t_oven)), "white")
			x += 1
			file.write(str(toCelsius(t_oven)))
			file.write("\n")


		# inputString = inputBox.getText()

		# if (len(inputString) > 1):
		# 	t_set = toKelvin(float(inputString))
		# elif inputString is "q" :
		# 	win.close()
		# 	break
		#######################################################################

		# target_deriv = computeTargetDerivative(t_oven, t_set)

		# if (deriv_oven < target_deriv):
		# 	duty = 1
		# else:
		# 	duty = 0

		####################################################################### 
		keyString = win.checkKey()

		if (keyString is "p"):
			duty = 1.0 - duty

		if (keyString is "q"):
			win.close()
			file.close()
			break


if __name__ == "__main__":
	main()
