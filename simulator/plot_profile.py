import sys
import matplotlib.pyplot as pyplot
import numpy


if (len(sys.argv) == 2):
	fname = sys.argv[1]
else:
	fname = "file.txt"


data = numpy.loadtxt(fname)
time = numpy.empty(len(data)-1)

for i in range(0, len(data)-1):
	time[i] = i * data[0];

pyplot.plot(time[0:], data[1:])

#pyplot.plotfile(fname, delimiter=' ', subplots=True, cols=(0,))
pyplot.show()