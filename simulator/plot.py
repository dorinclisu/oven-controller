import sys
import matplotlib.pyplot as pyplot


if (len(sys.argv) == 2):
	fname = sys.argv[1]
else:
	fname = "file.txt"


pyplot.plotfile(fname, delimiter='\t', subplots=True, cols=(0,1,2))
pyplot.show()