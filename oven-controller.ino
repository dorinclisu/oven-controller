/*
MIT License

Copyright (c) 2018 Dorin Clisu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "Parameters.h"
//#include "MAX31856/MAX31856.h"
#include "CAP1298.h"
#include "MovingAverage.hpp"
#include "OvenRegistry.h"
#include "UIManager.h"
#include "Display.h"
#include "Profile.h"
#include "Control.h"
#include "Memory.h"

// TODO:
// fix oled i2c lockup
// add main loop timeout for temperature measurement and raise error
// add temperature fault reading and raise error
// wait for first profile point reach
// wait for max profile point reach
// double click to start/stop
// fix touch 4 seconds hold limitation

// LAUNCHED TODO:
// make ssd1306 library
// make max31856 library
// cleanup file inclusion


// volatile bool touchEvent;
// void touchEventIRQ()
// {
// 	touchEvent = true;
// }

void setup()
{
	pinMode(MAX31856_NCS_PIN, OUTPUT);
	pinMode(MAX31856_SCK_PIN, OUTPUT);
	pinMode(MAX31856_SDI_PIN, OUTPUT);
	pinMode(MAX31856_SDO_PIN, INPUT);
	pinMode(MAX31856_NDRDY_PIN, INPUT);

	pinMode(CAP1298_ALERT_PIN, INPUT_PULLUP);

	pinMode(SOUND_PIN, OUTPUT);
	pinMode(HEAT_PIN, OUTPUT);
	pinMode(FAN_PIN, OUTPUT);
	pinMode(LED_PIN, OUTPUT);

	TCCR1B = TCCR1B & 0b11111000 | 0x02; //make pwm frequency 4khz on pin 10

	// for (int i=0; i<1000; i++)
	// {
	// 	digitalWrite(SOUND_PIN, HIGH);
	// 	delayMicroseconds(120);
	// 	digitalWrite(SOUND_PIN, LOW);
	// 	delayMicroseconds(120);
	// }
	///////////////////////////////////

	touch.init();
	touch.set_irq_on_release(false);
	touch.set_repeat_inputs(0b01100000);
	touch.set_enabled_inputs(0b11110111);
	touch.set_repeat_period(1);
	//touch.set_digital_gain(5);
	touch.set_average_sample_count(1);
	touch.set_sampling_time(0);
	touch.set_cycle_time(1);
	//check max button hold time problem !!!!!!
	///////////////////////////////////

	sensor.begin();
	sensor.setTempFaultThreshholds(5, TEMP_MAX + 5);
	sensor.setColdJunctionFaultThreshholds(5, 60);
	sensor.oneShotTemperature();
	sensor.readThermocoupleTemperature();
	///////////////////////////////////
	display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
	display.setCursor(0, 56);
	display.setTextColor(WHITE, BLACK);
	display.print(VERSION);
	display.display(); //show Adafruit Industries splashscreen - required as part of the BSD license

	analogWrite(SOUND_PIN, 128);
	delay(1000);
	analogWrite(SOUND_PIN, 0);

	display.clearDisplay();
	// display.drawBitmap(31, 16, logo, logo_sizeX, logo_sizeY, WHITE);
	// display.display();
	// delay(2000);
	// display.clearDisplay();
	display.dim(true);
	///////////////////////////////////

	Serial.begin(9600);

	loadSettings();
	if (settings.signature != MEM_SIGNATURE)
	{
		settings.signature = MEM_SIGNATURE;
		settings.speed = 1;
		settings.deadzone = 0;
		settings.useLearnedControl = false;
		settings.showDerivative = true;
		settings.timer = 0;
		settings.useFahrenheit = false;

		settings.learned = false;
		saveSettings();
		saveLearning();
	}

	if (settings.learned)
		loadLearning();

	// strcpy(profile.name, "default");
	// state.profiles_cursor = 1;
	// saveProfile();

	state.loadProfile = false;
	state.settings_cursor = 0;
	state.profiles_cursor = 0;
	state.settings_selected = false;
	state.saveSettings = false;
	state.running = false;
	state.heatON = false;
	state.fanON = true;
	state.mode = State::manual;
	state.wasRunning = false;
	state.beeping = false;

	control.temp_target = TEMP_MIN;
	control.temp_deriv_target = -1;

	for (int i=0; i < TEMP_FILTER_LEN; i++)
		readTemperature();

	///////////////////////////////////
	attachInterrupt(digitalPinToInterrupt(CAP1298_ALERT_PIN), handleTouchEvent, FALLING);
	attachInterrupt(digitalPinToInterrupt(MAX31856_NDRDY_PIN), readTemperature, FALLING);
}

///////////////////////////////////////////////////////////////////////////////
void loop()
{
	// if (touchEvent)
	// {
	// 	touchEvent = false;
	// 	handleTouchEvent();
	// }

	if (state.loadProfile)
	{
		state.loadProfile = false;
		loadProfile();
		if (profile.name[0] == '-') //not a valid choice
		{
			state.mode = State::profileChoice;
			state.clearDisplay = true;
			state.refreshDisplay = true;
		}
	}

	if (state.saveSettings)
	{
		state.saveSettings = false;
		saveSettings();
	}

	// if (state.clickSound)
	// {
	// 	state.clickSound = false;
	// 	clickSound();
	// }

	if (state.refreshDisplay)
	{
		if (state.clearDisplay)
		{
			state.clearDisplay = false;
			display.clearDisplay();
		}

		state.refreshDisplay = false;
		refreshDisplay();
	}

	if (state.running)
		digitalWrite(LED_PIN, HIGH);
	else if (state.beeping and ((millis() / BEEP_INTERVAL) & 1))
	{
		analogWrite(SOUND_PIN, 128);
		digitalWrite(LED_PIN, HIGH); 
	}
	else
	{
		analogWrite(SOUND_PIN, 0);
		digitalWrite(LED_PIN, LOW);
	}


	switch (state.mode)
	{
	case State::manual:
		if (Serial.available())
		{
			while (Serial.available())
				Serial.read();
			Serial.print(F("\nt0\tk0x100\n"));
			for (int i=0; i < LEARN_NODE_COUNT; i++)
			{
				Serial.print(learning.t [0][i]);
				Serial.print('\t');
				Serial.println(100*learning.k [0][i]);
			}
			Serial.print(F("t1\tk1x100\n"));
			for (int i=0; i < LEARN_NODE_COUNT; i++)
			{
				Serial.print(learning.t [1][i]);
				Serial.print('\t');
				Serial.println(100*learning.k [1][i]);
			}
		}

		if (state.tempReady)
		{
			state.tempReady = false;

			if (state.running)
			{
				if (not state.wasRunning)
				{
					analogWrite(SOUND_PIN, 128);
					delay(100);
					analogWrite(SOUND_PIN, 0);
					state.start_time = millis();
				}

				uint16_t elapsed = (millis() - state.start_time) / 1000;
				if (settings.timer > 0)
				{
					if (elapsed > settings.timer)
					{
						state.running = false; //// careful with that (axe eugene)
						state.beeping = true;
					}
					else
						elapsed = settings.timer - elapsed;
				}
				updateTimer(elapsed);
			}
			state.wasRunning = state.running;

			control.temp_deriv_target = getOptimumDerivative(control.temp_target, state.temp);
			applyControl();
			state.refreshDisplay = true;
			log();
		}	
		break;

	case State::profileChoice:
		if (Serial.available())
			downloadProfile(); //overwrite profile at state.profile_cursor
		break;

	case State::profile:
		if (state.tempReady)
		{
			state.tempReady = false;

			uint16_t elapsed = 0;
			uint16_t duration = profile.interval * (profile.length - 1);
			control.temp_target = profile.temp [profile.length-1];

			if (state.running)
			{
				if (not state.wasRunning)
				{
					analogWrite(SOUND_PIN, 128);
					delay(100);
					analogWrite(SOUND_PIN, 0);
					state.start_time = millis();
				}
				// if (not started)
				// 	state.start_time = millis();

				elapsed = (millis() - state.start_time) / 1000;

				if (runTemperatureProfile())
				{
					state.running = false;
					state.beeping = true;
				}
			}
			state.wasRunning = state.running;

			updateTimer(duration - elapsed);

			control.temp_deriv_target += getOptimumDerivative(control.temp_target, state.temp);
			applyControl();
			state.refreshDisplay = true;
			log();
		}
		break;

	case State::settings:
		break;

	case State::learning:
		learn();
		state.beeping = true;
		state.mode = State::settings;
		break;
	}
}
///////////////////////////////////////////////////////////////////////////////



void updateTimer(uint16_t elapsed)
{
	state.seconds = (elapsed) % 60;
	state.minutes = (elapsed) / 60;
	state.hours = state.minutes / 60;
	state.minutes -= state.hours * 60;
}


void tempError()
{
	display.clearDisplay();
	display.setCursor(0, 0);
	display.setTextSize(1);
	display.setTextColor(WHITE, BLACK);
	display.print(F("TEMP SENSOR ERROR"));
	display.display();
	state.mode = State::error;
	heatOFF();
	while (true) ; //never return //device must be powered off and repaired
}


void log()
{
	// Serial.print(millis() / 1000.0);
	// Serial.print('\t');
	Serial.print(state.temp_raw);
	Serial.print('\t');
	Serial.print(state.temp);
	// Serial.print('\t');
	// Serial.print(state.temp_deriv);
	Serial.print('\t');
	Serial.print(control.temp_target);
	// Serial.print('\t');
	// Serial.print(state.heatON);

	Serial.print('\n');
}


void clickSound()
{
		digitalWrite(SOUND_PIN, HIGH);
		delayMicroseconds(200);
		digitalWrite(SOUND_PIN, LOW);
		delayMicroseconds(200);
		digitalWrite(SOUND_PIN, HIGH);
		delayMicroseconds(200);
		digitalWrite(SOUND_PIN, LOW);
		// delayMicroseconds(200);
		// digitalWrite(SOUND_PIN, HIGH);
		// delayMicroseconds(200);
		// digitalWrite(SOUND_PIN, LOW);
}


void readTemperature()
{
	state.temp_raw = sensor.readThermocoupleTemperature();
	state.temp = tfilter.getAverage(state.temp_raw);
	state.tempReady = true;
	readDerivative();
}


void readDerivative()
{
	static uint32_t millis_prev = 0;
	uint32_t millis_now = millis();
	uint32_t dt = millis_now - millis_prev;
	millis_prev = millis_now;

	static float temp_prev = 0;
	float temp_deriv_raw = (state.temp - temp_prev) * 1000 / (dt + 1);
	temp_prev = state.temp;

	state.temp_deriv = tdfilter.getAverage(temp_deriv_raw);
}

inline void heatON() {digitalWrite(HEAT_PIN, HIGH);	state.heatON = true;}
inline void heatOFF() {digitalWrite(HEAT_PIN, LOW);	state.heatON = false;}
