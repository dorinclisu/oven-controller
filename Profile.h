#pragma once
#ifndef _PROFILE_H
#define _PROFILE_H

#include "OvenRegistry.h"


void downloadProfile()
{
	Serial.setTimeout(1000);

	bool error = false;
	memset(profile.name, ' ', 16);
	Serial.println();

	if (Serial.readBytesUntil('\n', profile.name, 16) < 3)
	{
		Serial.print(F("Name minimum 3 chars!\n"));
		error = true;
	}
	profile.name[15] = '\0';
	profile.interval = Serial.parseInt();

	uint8_t temp_prev;
	uint8_t index = 0;

	while (true)
	{
		uint8_t temp = Serial.parseInt();

		if (temp == 0)
		{
			if (index < 2)
			{
				Serial.print(F("Minimum 2 points!\n"));
				error = true;
			}
			break;
		}

		if (temp < TEMP_MIN)
		{
			Serial.print(F("TEMP_MIN="));
			Serial.println(TEMP_MIN);
			error = true;
			break;
		}
		if (temp > TEMP_MAX)
		{
			Serial.print(F("TEMP_MAX="));
			Serial.println(TEMP_MAX);
			error = true;
			break;
		}
		if (temp < temp_prev)
		{
			Serial.print(F("Curve must be strictly ascending!\n"));
			error = true;
			break;
		}

		if (index == 64)
		{
			Serial.print(F("Maximum 64 points!\n"));
			error = true;
			break;
		}

		profile.temp [index] = temp;
		index++;
		profile.length = index;
		temp_prev = temp;
#ifdef DEBUG
		Serial.println(temp);
#endif
	}


	if (error)
	{
		Serial.print(F("Download failed!\n"));
		loadProfile();
	}
	else
	{
		saveProfile();
		Serial.print(F("Download successful!\n"));
	}
}


bool runTemperatureProfile()
{
	uint32_t elapsed_ms = millis() - state.start_time;
	uint8_t index = elapsed_ms / (1000UL * profile.interval);

	uint32_t checkpoint_ms = (1000UL * profile.interval) * index;

	if (index == profile.length-1)
		return true;

	uint8_t t0 = profile.temp [index];
	uint8_t t1 = profile.temp [index+1];

	control.temp_target = t0 + static_cast<float> (t1-t0) * (elapsed_ms - checkpoint_ms) / (1000UL * profile.interval);
	control.temp_deriv_target = static_cast<float> (t1-t0) / profile.interval;
	return false;
}


#endif//_PROFILE_H