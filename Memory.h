#pragma once
#ifndef _MEMORY_H
#define _MEMORY_H

#include <EEPROM.h>
//#include <Arduino.h>
#include "OvenRegistry.h"
#include "Display.h"



void saveSettings()
{
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE, BLACK);
	display.setCursor(0, 0);
	display.println(F("SAVING SETTINGS..."));
	display.display();
	delay(500); //not really needed - it's a safety measure against bugs that might damage the memory with too many writes
	state.clearDisplay = true;
	state.refreshDisplay = true;

	noInterrupts();
	EEPROM.put(settingsAddress, settings);
	interrupts();
}

void loadSettings()
{
	noInterrupts();
	EEPROM.get(settingsAddress, settings);
	interrupts(); //TODO: maybe it's better to restore the interrupts flag instead of simply enabling them
}


void saveLearning()
{
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE, BLACK);
	display.setCursor(0, 0);
	display.println(F("SAVING LEARNING..."));
	display.display();
	delay(500); //not really needed - it's a safety measure against bugs that might damage the memory with too many writes
	state.clearDisplay = true;
	state.refreshDisplay = true;

	noInterrupts();
	EEPROM.put(learningAddress, learning);
	interrupts();
}

void loadLearning()
{
	noInterrupts();
	EEPROM.get(learningAddress, learning);
	interrupts();
}


void saveProfile()
{
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE, BLACK);
	display.setCursor(0, 0);
	display.println(F("SAVING PROFILE..."));
	display.display();
	delay(500); //not really needed - it's a safety measure against bugs that might damage the memory with too many writes
	state.clearDisplay = true;
	state.refreshDisplay = true;
	
	noInterrupts();
	EEPROM.put(profilesAddress + state.profiles_cursor * sizeof(profile), profile);
	interrupts();
}

void loadProfileName(uint8_t cursor)
{
	noInterrupts();
	EEPROM.get(profilesAddress + cursor * sizeof(profile), profile.name);
	interrupts();
	if (profile.name[0] <= 0)
		strcpy(profile.name, "-");
}

void loadProfile()
{
	noInterrupts();
	EEPROM.get(profilesAddress + state.profiles_cursor * sizeof(profile), profile);
	interrupts();
	if (profile.name[0] <= 0)
		strcpy(profile.name, "-");
}


#endif//_MEMORY_H