#pragma once
#ifndef _MOVINGAVERAGE_HPP
#define _MOVINGAVERAGE_HPP

typedef unsigned int uint;


class MovingAverage
{
public:
	MovingAverage(float* buffer, const uint length) 
	{
		buff = buffer;
		len = length;
		head = 0;
		sum = 0;
		clear();
	}

	~MovingAverage() {}

	void clear()
	{
		memset(buff, 0, len * sizeof(float));
	}

	void push(float value)
	{
		sum += value - buff[head];
		buff[head] = value;
		head = ((len-1) == head) ? 0 : (head+1);
	}

	float getSum(float value)
	{
		push(value);
		return sum;
	}

	float getAverage(float value)
	{
		push(value);
		return sum / static_cast<float> (len);
	}

private:
	float* buff;
	uint len;
	uint head;
	float sum;
};



// class RingBuffer
// {
// public:
// 	RingBuffer(float* buffer, const uint length)
// 	{
// 		buff = buffer;
// 		len = length;
// 		head = 0;
// 		clear();
// 	}

// 	void clear()
// 	{
// 		memset(buff, 0, len * sizeof(float));
// 	}

// 	float push(float value)
// 	{
// 		float oldest = buff[head];
// 		buff[head] = value;
// 		head = ((len-1) == head) ? 0 : (head+1);
// 		return oldest;
// 	}

// 	float at(uint index) //index=0 returns newest, index=len-1 returns oldest
// 	{
// 		if (index > len-1)
// 			return 0;

// 		return buff[(head + len-1 - index) % len];
// 	}

// private:
// 	float* buff;
// 	uint len;
// 	uint head;
// };


#endif//_MOVINGAVERAGE_HPP